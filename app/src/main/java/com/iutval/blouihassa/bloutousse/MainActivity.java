package com.iutval.blouihassa.bloutousse;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import fr.iutvalence.android.BTConnectionHandlerLib.BTConnectionHandler;
import fr.iutvalence.android.BTConnectionHandlerLib.exceptions.BTHandlingException;


public class MainActivity extends Activity {
    
    private BTConnectionHandler btc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.btc = new BTConnectionHandler(this);
    }

    public void makeConnection(View v) throws IOException, BTHandlingException {

        EditText deviceName = (EditText)findViewById(R.id.deviceName);
        try {
            this.btc.connectToBTDevice(deviceName.getText().toString());
            TextView status = (TextView) findViewById(R.id.status);
            status.setText("Status: Connect");
        } catch (BTHandlingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void makeDisconnection(View v){
        this.btc.closeConnection();
        TextView status = (TextView) findViewById(R.id.status);
        status.setText("Status: Disconnect");
    }

    public void sendCommand(View v){
        //EditText command = (EditText)findViewById(R.id.command);
        try {
            this.btc.sendData(v.getTag().toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BTHandlingException e) {
            e.printStackTrace();
        }

    }
    
}
